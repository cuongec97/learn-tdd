// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/foundation.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:test_tdd/main.dart';

void main() {
  group("Widgets test", () {
    testWidgets("Hien thi textfield", (WidgetTester tester) async {
      await tester.pumpWidget(MyApp());

      expect(find.byKey(Key("num1")), findsOneWidget);
      expect(find.byKey(Key("num2")), findsOneWidget);
    });
  });
}
