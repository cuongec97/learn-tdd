import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          width: MediaQuery.of(context).size.width * 0.3,
          child: Column(
            children: [
              TextField(
                key: Key("num1"),
                decoration: InputDecoration(
                  hintText: 'Enter number 1',
                ),
              ),
              TextField(
                key: Key("num2"),
                decoration: InputDecoration(
                  hintText: 'Enter number 2',
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
